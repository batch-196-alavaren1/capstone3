const productsData = [
	{
		id: "wdc001",
		name: "ASUS ROG",
		description: "GeForce RTX™ 3050 Ti Laptop GPU,Windows 11 Pro,AMD Ryzen™ 9 6900HS,ROG Nebula HDR Display,16 inch, QHD+ 16:10 (2560 x 1600, WQXGA), Refresh Rate:165Hz,1TB PCIe® 4.0 NVMe™ M.2 SSD", 
		price: 95000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Acer Predator",
		description: "Acer Predator G9-593 is a Windows 10 Home laptop with a 15.60-inch display that has a resolution of 1920x1080 pixels. It is powered by a Core i7 processor and it comes with 16GB of RAM. The Acer Predator G9-593 packs 1TB of HDD storage and 256GB of SSD storage. Graphics are powered by Nvidia GeForce GTX 1060.",
		price: 75000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Lenove Legion",
		description: "Legion Slim 7i Gen ,Thin & powerful with 12th Gen Intel® Core Processors,Play on max settings with NVIDIA® GeForce RTX™ 30 Series GPU's,Top-flight visuals with several 16″ WXQGA display options",
		price: 85000,
		onOffer: true
	},

]

export default productsData;