import {useState, useContext} from 'react';
import {Link} from 'react-router-dom';
import {Navbar, Container, Nav} from 'react-bootstrap';
import UserContext from '../UserContext';

export default function AppNavbar(){
	
	
	const {user} = useContext(UserContext);
	console.log(user);

	return (
	<Navbar className="nav" expand="lg">
		<Container>
		    <Navbar.Brand className="logo" id="logoBtn"as={Link} to="/"><h1>CLI</h1></Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          <Nav className="me-auto">
		            <Nav.Link className="link" as={Link} to="/">Home</Nav.Link>
		             	<Nav.Link className="link"  as={Link} to="/products">Products</Nav.Link>

		             	{
		             		(user.id !== null) ?
		             		<>
		             			<Nav.Link className="link"  as={Link} to="/dashboard">DashBoard</Nav.Link>
		             			<Nav.Link className="link"  as={Link} to="/profile">View Profile</Nav.Link>
		             			<Nav.Link className="link"  as={Link} to="/logout">Logout</Nav.Link>
		             		</>
		             		:
		             	  	<>
				             	 <Nav.Link className="link"  as={Link} to="/login">Login</Nav.Link>
				             	 <Nav.Link className="link"  as={Link} to="/register">Register</Nav.Link>
		             	 	</>
		             	}
		            		        
		          </Nav>
		        </Navbar.Collapse>
		</Container>
	</Navbar>
		)
};
