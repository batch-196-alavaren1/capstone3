import {useState, useEffect, useContext} from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView(){
	

	const {user} = useContext(UserContext);

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [isAdmin, setIsAdmin] = useState("");


	const {productId} = useParams();
	const history = useNavigate();

	const order = (productId) => {
		fetch('https://dry-crag-83742.herokuapp.com/orders',{
			method: "POST",
			body: JSON.stringify({
				totalAmount: price,
				userId: user.id,
				productId,
				quantity: 1,
			}),
			headers: {
				'Content-Type' : 'application/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`
			},
		})
		.then(res => res.json())
		.then(res => {
			
			if(user.isAdmin !== true){
			 	Swal.fire({
			 		title: res.message,
			 		icon: 'success',
			 		text: 'Thank you for processing your order.'
			 	})

			 	history("/products");

			} else {
			 	Swal.fire({
			 		title: 'Admin unable to process order.',
					icon: 'error',
			 		text: 'Your are not allowed to process order.'
				})
			}


		})
	}

	useEffect(()=> {
		console.log(productId)
		fetch(`https://dry-crag-83742.herokuapp.com/products/product/${productId}`)
		.then(res => res.json())
		.then(data =>{
			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})

	}, [productId]);

		return(
			<Container className="pview mt-5">
				<Row>
					<Col lg={{span:6, offset:3}}>
						<Card>
							<Card.Body>
								<Card.Title>{name}</Card.Title>
								<Card.Subtitle>Description:</Card.Subtitle>
								<Card.Text>{description}</Card.Text>
								<Card.Subtitle>Price:</Card.Subtitle>
								<Card.Text>Php {price}</Card.Text>
								{ user.id !== null ?
								 	<Button variant="primary" onClick={() => order(productId)}>Order</Button>
								 		
								 		:
								 	<Link className="btn btn-danger" to="/Login">Log In</Link>
								}
							</Card.Body>
						</Card>
					</Col>
				</Row>
			</Container>
		)
	};