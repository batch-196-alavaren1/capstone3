import {useState} from 'react';
import {Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function ProductCard({productProp}){

	const {name, description, price, _id} = productProp;

		return (
			<Card>
			      <Card.Body>
			        <Card.Title>{name}</Card.Title>
			        <Card.Subtitle>Product Description</Card.Subtitle>
			        <Card.Text>
			          {description}
			        </Card.Text>
			        <Card.Subtitle>Product Price</Card.Subtitle>
			        <Card.Text>
			          {price}
			        </Card.Text>
			        <Link className="btn btn-primary" to={`/productView/${_id}`}>View Details</Link>
			      </Card.Body>
			    </Card>
		)
	};