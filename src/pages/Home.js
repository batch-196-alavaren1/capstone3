import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {
		const data = {
	        title: "So what are you waiting for?",
	        content: "Get one of our coolest gadget in as fast as two days.",
	        destination: "/products",
	        label: "Order now!"
	    }

    return (
        <>
        	<Highlights />
	        <Banner data={data}/>
	        
		</>

    )
}

