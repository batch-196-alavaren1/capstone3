import {useState, useEffect} from 'react';
import productsData from '../data/productsData';
import ProductCard from '../components/ProductCard';

export default function Products(){
	
	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch("https://dry-crag-83742.herokuapp.com/products/activeProducts_avail")
		.then(res => res.json())
		.then(data => {
			setProducts(data.map(product => {
				return (
					<ProductCard key={product._id} productProp = {product}/>
				)
			}));
		});


	}, []);

	return(
		<>
		   <div className="allpdt">
				<h1 className="prdct">Available Products:</h1>
				<div className="pList">
				{products}
				</div>
		   </div>
		</>	

	)
}