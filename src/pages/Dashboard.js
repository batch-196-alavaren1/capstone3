import {useState, useContext, useEffect } from 'react';
import { Form, Table, Button, Modal, Accordion, Card } from 'react-bootstrap';
import {Link, Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Dashboard() {
  
  
  const {user, setUser} = useContext(UserContext);
  const [products, setProducts ] = useState([]);
  const [id, setId] = useState("");
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [showAdd, setShowAdd] = useState(false);
  const [showEdit, setShowEdit] = useState(false);


  // const [isActive, setIsActive] = useState(true);
  const [toggle, setToggle] = useState(false);


  const openAdd = () => setShowAdd(true);
  const closeAdd = () => setShowAdd(false);


  const openEdit = (productId) => {
      setId(productId);
      fetch(`https://dry-crag-83742.herokuapp.com/products/product/${ productId }`)
      .then(res => res.json())
      .then(data => {
          setName(data.name);
          setDescription(data.description);
          setPrice(data.price);
      });
      setShowEdit(true);
  };

  const closeEdit = () => {
      setName("");
      setDescription("");
      setPrice(0);
      setShowEdit(false);

  };


  const addProduct = (e) => {
      e.preventDefault();
      fetch(`https://dry-crag-83742.herokuapp.com/products/`, {
          method: 'POST',
          headers: {
              Authorization: `Bearer ${ localStorage.getItem('token') }`,
              'Content-Type': 'application/json'
          },
          body: JSON.stringify({
              name: name,
              description: description,
              price: price
          })
      })
      .then(res => res.json())
      .then(data => {
          if (data) {

              Swal.fire({
                title: data.name.toUpperCase(),
                icon: 'success',
                text: 'has been successfully added!'
              })
              setName("");
              setDescription("");
              setPrice(0);
              closeAdd();

              LoadProducts();
          } else {
              closeAdd();
          }
      });
  };

  const editProduct = (e, productId) => {
    e.preventDefault();
    fetch(`https://dry-crag-83742.herokuapp.com/products/update/${ productId }`, {
        method: 'PUT',
        headers: {
            Authorization: `Bearer ${ localStorage.getItem('token') }`,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            name: name,
            description: description,
            price: price
        })
    })
    .then(res => res.json())
    .then(data => {
        if (data) {
          Swal.fire({
            title: data.name.toUpperCase(),
            icon: 'success',
            text: 'has been successfully updated'
          })
            setName("");
            setDescription("");
            setPrice(0);
            closeEdit();

            LoadProducts();
        } else {

            closeEdit();
        }
    });
};

const deleteProduct = (productId) => {
  
  fetch(`https://dry-crag-83742.herokuapp.com/products/delete/${productId}`, {
    method: 'DELETE',
    headers: {
         Authorization: `Bearer ${localStorage.getItem('token') }`
    }
    
    })
  .then(res => res.json())
  .then(data => {
    if(data) {
      Swal.fire({
        title: data.name.toUpperCase(),
        icon: 'success',
        text: 'has been successfully deleted!'
      })

      LoadProducts();
    } else {
      Swal.fire({
        title: "Unsuccessful",
        icon: 'error',
        text: 'Something went wrong.'
      })
    }
  })
  }

 const toggler = () => {
     if(toggle === true){
         setToggle(false);
     }else{
         setToggle(true);
     }
 };

 /**/
  const activateProduct = (productId) => {
    fetch(`https://dry-crag-83742.herokuapp.com/products/activate/${productId}`, {
      method: 'PUT',
      headers: {
            Authorization: `Bearer ${localStorage.getItem('token') }`
      }
    })
    .then(res => res.json())
    .then(data => {
        if(data) {
          Swal.fire({
            title: data.name.toUpperCase(),
            icon: 'success',
            text: 'has been successfully activated!'
          })
                LoadProducts();
        } else {
          Swal.fire({
            title: "Unsuccessful",
            icon: 'error',
            text: 'Something went wrong.'
          })
        }
    })
  }

 const archiveProduct = (productId) => {
   fetch(`https://dry-crag-83742.herokuapp.com/products/archive/${productId}`, {
     method: 'PUT',
     headers: {
           Authorization: `Bearer ${localStorage.getItem('token') }`
     }
   })
   .then(res => res.json())
   .then(data => {
       if(data) {
         Swal.fire({
           title: data.name.toUpperCase(),
           icon: 'success',
           text: 'has been successfully archived!'
         })
          LoadProducts();
       } else {
         Swal.fire({
           title: "Unsuccessful",
           icon: 'error',
           text: 'Something went wrong.'
         })
       }
   })
 }

 const LoadProducts = () => {
    fetch("https://dry-crag-83742.herokuapp.com/products/activeProducts")
    .then(res => res.json())
    .then(data => {
      setProducts(data.map(({_id, name, price, description, isAvail}, index) => {
        return (
         
          <tr key={_id}>
           <td><Link to={`/productView/${_id}`}>{index + 1}</Link></td>
           <td>{name}</td>
           <td>{description}</td>
           <td>{`Php ${price}`}</td>
           <td>
           { isAvail === true ? 
            <span>Available</span>
            :
            <span>Unavailable</span>
           }
           </td>
           <td>
           <Button variant="outline-primary" onClick={() => openEdit(_id)}>Edit</Button>
           <Button variant="outline-danger" onClick={() => deleteProduct(_id)}>Delete</Button>
           {
            isAvail === true ?
            <Button variant="outline-secondary" onClick={() => archiveProduct(_id)}>Disable</Button>
            :
            <Button variant="outline-warning" onClick={() => activateProduct(_id)}>Enable</Button>
           }

           </td>
         </tr>

         )
      }))
    });
 }


  useEffect(() => {
    LoadProducts();

  }, []);

  return (
    (user.isAdmin !== true) ?
     <Navigate to='/'/>
    : 
    <>
      
      <div className="text-center my-4">
          <h2 className="dash">Admin Dashboard</h2>
          <div className="d-flex justify-content-center">
              <Button className="mr-1" variant="primary"onClick={openAdd}>
                  Add New Product
              </Button>
          </div>
      </div>

      
          <Table striped bordered hover>
             <thead>
               <tr className="table">
                 <th>#</th>
                 <th>Name</th>
                 <th>Description</th>
                 <th>Price</th>
                 <th>Availability</th>
                 <th>Actions</th>
               </tr>
             </thead>
             <tbody className="tbody">
              {products.length >= 0 ? products : "No products listed"}
             </tbody>
          </Table>


     
      <Modal show={showAdd} onHide={closeAdd}>
        <Form onSubmit={e => addProduct(e)}>
          <Modal.Header closeButton>
            <Modal.Title>Add New Product</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group controlId="productName">
                <Form.Label>Name:</Form.Label>
                <Form.Control 
                    type="text"
                    placeholder="Enter product name"
                    value={name}
                    onChange={e => setName(e.target.value)}
                    required
                />
            </Form.Group>
            <Form.Group controlId="productDescription">
                <Form.Label>Description:</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Enter product description"
                    value={description}
                    onChange={e => setDescription(e.target.value)}
                    required
                />
            </Form.Group>
            <Form.Group controlId="productPrice">
              <Form.Label>Price:</Form.Label>
              <Form.Control
                type="number"
                value={price}
                onChange={e => setPrice(e.target.value)}
                required
              />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={closeAdd}>
                Close
            </Button>
            <Button variant="success" type="submit">
                Submit
            </Button>
          </Modal.Footer>
        </Form> 
      </Modal>

      <Modal show={showEdit} onHide={closeEdit}>
        <Form onSubmit={e => editProduct(e, id)}>
          <Modal.Header closeButton>
              <Modal.Title>Edit Product</Modal.Title>
          </Modal.Header>
          <Modal.Body>

                  <Form.Group controlId="productName">
                      <Form.Label>Name:</Form.Label>
                      <Form.Control
                          type="text"
                          placeholder="Enter product name"
                          value={name}
                          onChange={e => setName(e.target.value)}
                          required
                      />
                  </Form.Group>

                  <Form.Group controlId="productDescription">
                      <Form.Label>Description:</Form.Label>
                      <Form.Control
                          type="text"
                          placeholder="Enter product description"
                          value={description}
                          onChange={e => setDescription(e.target.value)}
                          required
                      />
                  </Form.Group>

                  <Form.Group controlId="productPrice">
                      <Form.Label>Price:</Form.Label>
                      <Form.Control
                          type="number"
                          placeholder="Enter product price"
                          value={price}
                          onChange={e => setPrice(e.target.value)}
                          required
                      />
                  </Form.Group>
          </Modal.Body>
          <Modal.Footer>
              <Button variant="secondary" onClick={closeEdit}>
                  Close
              </Button>
              <Button variant="success" type="submit">
                  Submit
              </Button>
          </Modal.Footer>
        </Form> 
      </Modal>

    </>
  )
}

