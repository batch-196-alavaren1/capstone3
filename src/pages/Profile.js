import {useState, useEffect} from 'react';
import {Card, Alert, Button} from 'react-bootstrap';

export default function Profile(){
	
	const [show, setShow] = useState(true);

  	const [Profile, SetProfile] = useState({
  		email : "",
  		firstName : "",
  		lastName : "",
  		mobileNo : ""
  	});


	useEffect(() => {
		var token = localStorage.getItem('token');

		fetch('https://dry-crag-83742.herokuapp.com/users/details', {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// if (data){
			// 	alert("login ak");
			// } else {
			// 	alert("dre naka login");
			// }


			SetProfile({
		  		email : data.email,
		  		firstName : data.firstName,
		  		lastName : data.lastName,
		  		mobileNo : data.mobileNo
			});
		})
	}, []);







	return (
	<Card className="cardProfile p-3">
		      <Alert show={show} variant="success" >
		        <Alert.Heading>How are you today?</Alert.Heading>
		        <p>
		          Keep smiling, it feels great!
		        </p>
		        <hr />
		        <div className="d-flex justify-content-end">
		          <Button onClick={() => setShow(false)} variant="outline-success">
		            You can choose to close me, or not!
		          </Button>
		        </div>
		      </Alert>

		      {!show && <Button variant="info" onClick={() => setShow(true)}>Click Me Please!</Button>}
	

	   
	         <Card.Header ><h2 variant="success">YOUR PROFILE</h2></Card.Header>
	         <Card.Body>
	           <Card.Title>First Name:</Card.Title>
	           <Card.Text>
	              {Profile.firstName}
	           </Card.Text><hr/>
	           <Card.Title>Last Name:</Card.Title>
	           <Card.Text>
	              {Profile.lastName}
	           </Card.Text><hr/>
	           <Card.Title>Email Address:</Card.Title>
	           <Card.Text>
	              {Profile.email}
	           </Card.Text><hr/>
	           <Card.Title>Mobile Number:</Card.Title>
	           <Card.Text>
	              {Profile.mobileNo}
	           </Card.Text>
	         </Card.Body>
	    </Card>
	 );
}