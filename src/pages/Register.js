import {useState, useEffect, useContext} from 'react';
import {Form, Button, Col} from 'react-bootstrap';
import {Navigate, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register(){

	const {user, setUser} = useContext(UserContext);
	const history = useNavigate();

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	function registerUser(e){
		e.preventDefault();

		fetch('https://dry-crag-83742.herokuapp.com/users/checkEmailExists', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data){
				Swal.fire({
					title: "Duplicate email found",
					icon: "info",
					text: "The email that you're trying to register already exist"
				});
			} else {
				fetch('https://dry-crag-83742.herokuapp.com/users', {
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(data.email){
						Swal.fire({
							title: 'Registration successful!',
							icon: 'success',
							text: 'Thank you for registering'
						})
						history("/login");
					} else {
						Swal.fire({
							title: ' Registration failed',
							icon: 'error',
							text: 'Something went wrong, try again'
						})
					}
				});
			};
		});
		setEmail('');
		setPassword('');
		setFirstName('');
		setLastName('');
		setMobileNo('');
	};
	useEffect(() => {
		if(email !== '' && firstName !== '' && lastName !== '' && password !== '' && mobileNo !== '' && mobileNo.length === 11){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [email, firstName, lastName, mobileNo, password]);

	return (
			(user.id !== null) ?
				<Navigate to="/products/activeProducts"/>
			:
			<>
			<Col className="register" lg={{span:6, offset:3}}>
			<h1 className="regtitle">Register Here:</h1>
			<Form onSubmit={e => registerUser(e)}>
				<Form.Group controlId="firstName">
					<Form.Label className="regtxt">First Name</Form.Label>
					<Form.Control className="txt"
						type="firstName"
						placeholder="Enter your first name here"
						required
						value= {firstName}
						onChange= {e => setFirstName(e.target.value)} 
					/>
				</Form.Group>

				<Form.Group controlId="lastName">
					<Form.Label className="regtxt">Last Name</Form.Label>
					<Form.Control className="txt"
						type="lastfirstName"
						placeholder="Enter your last name here"
						required
						value= {lastName}
						onChange= {e => setLastName(e.target.value)}
					/>
				</Form.Group>

					<Form.Group controlId="mobileNo">
						<Form.Label className="regtxt">Mobile Number</Form.Label>
						<Form.Control className="txt"
							type="mobileNo"
							placeholder="Enter your mobile number here"
							required
							value= {mobileNo}
							onChange= {e => setMobileNo(e.target.value)}
						/>
				</Form.Group>

					<Form.Group controlId="userEmail">
						<Form.Label className="regtxt">Email Address</Form.Label>
						<Form.Control className="txt"
							type="email"
							placeholder="Enter your email here"
							required
							value= {email}
							onChange= {e => setEmail(e.target.value)}
						/>
				</Form.Group>

				<Form.Group controlId="password">
					<Form.Label className="regtxt">Password</Form.Label>
					<Form.Control className="txt"
						type="password"
						placeholder="Enter your password here"
						required
						value={password}
						onChange={e => setPassword(e.target.value)}
					/>
				</Form.Group>

				<p className="regtxt1">Already have an account? <Link to="/login">Click here</Link> to log in</p>


			{ isActive ?			
				<Button className="mt-3 mb-5" variant="danger" type="submit" id="submitBtn">
					Register
				</Button>
				:
				<Button className="mt-3 mb-5" variant="secondary" type="submit" id="submitBtn" disabled>
					Register
				</Button>
			}

			</Form>
			</Col>
			</>
		)
}