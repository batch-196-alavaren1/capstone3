import {useState, useEffect, useContext} from 'react';
import {Form, Button, Col} from 'react-bootstrap';
import {Navigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login(){

	
	const {user, setUser} = useContext(UserContext);
	console.log(user);


	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	function loginUser(e){
		e.preventDefault();

		fetch('https://dry-crag-83742.herokuapp.com/users/login', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(typeof data.accessToken !== "undefined"){
				localStorage.setItem('token', data.accessToken)
				getUserDetails(data.accessToken);

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome! How's it going?"
				})
			} else {

				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your credentials"
				})	
			}
		})
		setEmail('');
		setPassword('');
	};

		const getUserDetails = (token) => {

			fetch('https://dry-crag-83742.herokuapp.com/users/details', {
				headers: {
					Authorization: `Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)

				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				});
			})
		};

		useEffect(() => {

			if(email !== '' && password !== ''){
				setIsActive(true);
			} else {
				setIsActive(false);
			}
		}, [email, password]);

		return(
			(user.id !== null) ?
			<Navigate to='/products'/>
			:
			<>
			<Col className="login" lg={{span:6, offset:3}}>
			<h1 className="logtitle">Login Here:</h1>
			<Form  onSubmit={e => loginUser(e)}>
				<Form.Group controlId="loginEmail">
					<Form.Label className="logtext">Email address</Form.Label>
					<Form.Control className="text"
						type= "email"
						placeholder="Enter Email"
						required
						value= {email}
						onChange= {e => setEmail(e.target.value)}
					/>
				</Form.Group>

				<Form.Group controlId="loginPassword">
					<Form.Label className="logtext">Password</Form.Label>
					<Form.Control className="text"
						type= "password"
						placeholder="Enter Password"
						required
						value= {password}
						onChange= {e => setPassword(e.target.value)}
					/>
				</Form.Group>

				<p className="logtext3">Not yet registered? <Link to="/register">Register Here</Link></p>

				{ isActive ?
					<Button className="mt-3 mb-5" variant="danger" type="submit" id="submitBttn">Login</Button>
					:
					<Button className="mt-3 mb-5" variant="secondary"  type="submit" id="submitBttn" disabled>Login</Button>
				}

			</Form>
			</Col>
			</>

		)

	};