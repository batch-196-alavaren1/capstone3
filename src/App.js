import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import ProductView from './components/ProductView';
import Products from './pages/Products';
import Error from './pages/Error';
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import Dashboard from './pages/Dashboard';
import Profile from './pages/Profile';
import Logout from './pages/Logout';
import './App.css';
import {UserProvider} from './UserContext';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  };
  
  return (
    <>
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <AppNavbar/> 
          <Container>
            <Routes>
              <Route exact path="/" element={<Home/>}/>
              <Route exact path="/products" element={<Products/>}/>
              <Route exact path="/productView/:productId" element={<ProductView/>}/>
              <Route exact path="/login" element={<Login/>}/>
              <Route exact path="/register" element={<Register/>}/>
              <Route exact path="/dashboard" element={<Dashboard/>}/>
              <Route exact path="/profile" element={<Profile/>}/>
              <Route exact path="/logout" element={<Logout/>}/>
              <Route exact path="*" element={<Error/>}/>
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  )
}

export default App;
